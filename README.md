# Node task

Hello world

## Languages and frameworks

- Javascript
- NodeJs
- Express

![Express icon](express.png)

## Setup

1. Clone the repositoy ` git clone https://gitlab.com/caliagatorro/node-task.git `
2. Go into the project ` cd node-task `
3. Run the comand to start the server ` npm start `
4. Use your browser to test it with the following url ` http://localhost:3000 `

## Endpoints table

| Endpoints | Function                    | Parameters |
|-----------|-----------------------------|------------|
| /         | Print Hello World           | ---        |
| /mul      | Multiplication of 2 numbers | a & b      |

# Multiplication

Multiplication of two numbers

```
const mul = (a, b) => {
  const mul = a * b;
  return mul;
};
```
