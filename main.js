const express = require("express");
const app = express();
const PORT = 3000;

// Init of the server
console.log("Server-side program starting...");

// baseurl: localhost:3000
// Enpoint: localhost:3000/
app.get("/", (req, res) => {
  res.send("Hello World!");
});

/**
* This arrow function multiply two numbers
 * @param {Number} a first param
 * @param {Number} b second param
 * @returns {Number}
 */
const mul = (a, b) => {
  const mul = a * b;
  return mul;
};

// add endpoint: http://localhost:3000/mul?a=value&b=value
app.get("/mul", (req, res) => {
  console.log(req.query);
  const result = mul(req.query.a, req.query.b);
  res.send(result.toString());
});

app.listen(PORT, () =>
  console.log(`Server listening http://localhost:${PORT}`)
);
